jQuery(function ($) {
    $(document).ready(function () {
        //Upload image as test answer

        $('.test-table-constructor').on('change', '.test-answer', function (e) {
                let elemId = $(this).attr('id'),
                arr = elemId.split("_");
                arr[0] = "item";
            if (this.files[0]) { // если выбрали файл
                $('#' + arr.join('_')).val(this.files[0].name);
                var reader = new FileReader();
                reader.onload = function(e) {
                    var image = '<div class="img-answer" style="background-image: url(' + e.target.result +');"></div>'
                    if($('#' + arr.join('_')).next('.preview-img').length>0){
                        $('#' + arr.join('_')).next('.preview-img').html(image);
                    }
                    else {
                        $('<div class="preview-img">'+ image + '</div>').insertAfter('#' + arr.join('_'));
                    }
                };
                reader.readAsDataURL(e.target.files[0]);
                $(this).parents('.input-group').css("padding-bottom", "60px");
            }
        });

        //drag question
        $('.test-table-constructor .custom-table__body').sortable(
            {
                appendTo: ".custom-table__body",
                cancel: ".title, .circle-add, input, select, label",
                axis: "y",
                items: "> .custom-table__edit-row",
                deactivate: function (event, ui) {
                    RefreshQuestionCostructor();
                }
            }
        );
        function RefreshQuestionCostructor() {
            var questions = 0;
            var Sections = $('.test-table-constructor').find('.custom-table__body');
            Sections.each(function (index, section) {
                questions = 0;
                var Questions = $(section).children();
                Questions.each(function (index, element) {
                    var numbers = $(element).find('.input-cont-constructor-test span').text().split('/');
                    if (numbers[1]) {
                        questions++;
                        numbers[1] = questions;
                        $(element).find('.input-cont-constructor-test span').html(numbers.join('/'));
                    }
                    var inputs = $(element).find('input');
                    inputs.each(function (index, input) {
                        prevName = $(input).attr('name').split("_");
                        prevName[2] = questions;
                        newName = prevName.join('_');
                        $(input).attr('name', newName);
                        $(input).attr('id', newName);
                    });
                    var labels = $(element).find('label');
                    labels.each(function (index, label) {
                        if($(label).attr('for')){
                            prevName = $(label).attr('for').split("_");
                            prevName[2] = questions;
                            newName = prevName.join('_');
                            $(label).attr('for', newName);
                        }
                    });
                    var Selects = $(element).find('select');
                    Selects.each(function (index, select) {
                        prevName = $(select).attr('name').split("_");
                        prevName[2] = questions;
                        newName = prevName.join('_');
                        $(select).attr('name', newName);
                        $(select).attr('id', newName);
                    });
                    var Links = $(element).find('.add-questions-item');
                    Links.each(function (index, link) {
                        $(link).attr('data-question', questions);
                    });
                });
            });
        }
        $('.test-table-constructor select').customSelect();
        //add  questions item (radio)
        $('.test-table-constructor').on('click', '.add-questions-item', function () {
            var section = $(this).attr('data-section');
            var question = $(this).attr('data-question');
            var item = 1 + parseInt($(this).attr('data-item'));
            var newItem =
                '<label class="test-radio">' +
                '    <input type="checkbox" name="questionRadio_' + section + '_' + question + '_' + item + '" id="questionRadio_' + section + '_' + question + '_' + item + '" checked>' +
                '    <div class="test-radio__answer"></div>' +
                '    <label>' +
                '        <input type="text" class="answer input-shdw" value="" placeholder="Введите вариант ответа" name="item_' + section + '_' + question + '_' + item + '" id="item_' + section + '_' + question + '_' + item + '">' +
                '    </label>' +
                '    <input class="scores input-shdw" type="text" value="0" name="itemScore_' + section + '_' + question + '_' + item + '" id="itemScore_' + section + '_' + question + '_' + item + '">' +
                '</label>';
            var newInputfile = 
            '<div class="upload-answer">'+
            '    <div class="input-group">'+
            '        <input id="file-name_' + section + '_' + question + '_' + item + '" type="file" name="file-name_' + section + '_' + question + '_' + item + '" class="test-answer">'+
            '        <label for="file-name_' + section + '_' + question + '_' + item + '"><img class="upload-image" src="../../../img/corp/icons/download-arrow_blue.png"></label>'+
            '    </div>'+
            '</div>';
            $(this).attr('data-item', item);
            $(newItem).insertBefore($(this));
            $(newInputfile).appendTo($(this).parents('.custom-table__cell').next('.custom-table__cell'));
        });
        //add  questions item (check)
        $('.test-table-constructor').on('click', '.add-questions-item-check', function () {
            var section = $(this).attr('data-section');
            var question = $(this).attr('data-question');
            var item = 1 + parseInt($(this).attr('data-item'));
            var newItem =
                '<label class="test-radio">' +
                '    <input type="checkbox" name="questionRadio_' + section + '_' + question + '_' + item  + '" id="questionRadio_' + section + '_' + question + '_' + item + '" checked>' +
                '    <div class="test-radio__answer"></div>' +
                '    <label>' +
                '        <input type="text" class="answer input-shdw" placeholder="Введите вариант ответа" name="item_' + section + '_' + question + '_' + item + '" id="item_' + section + '_' + question + '_' + item + '">' +
                '    </label>' +
                '    <input class="scores input-shdw" type="text" value="0" name="itemScore_' + section + '_' + question + '_' + item + '" id="itemScore_' + section + '_' + question + '_' + item + '">' +
                '</label>';
            var newInputfile = 
                '<div class="upload-answer">'+
                '    <div class="input-group">'+
                '        <input id="file-name_' + section + '_' + question + '_' + item + '" type="file" name="file-name_' + section + '_' + question + '_' + item + '" class="test-answer">'+
                '        <label for="file-name_' + section + '_' + question + '_' + item + '"><img class="upload-image" src="../../../img/corp/icons/download-arrow_blue.png"></label>'+
                '    </div>'+
                '</div>';
            $(this).attr('data-item', item);
            $(newItem).insertBefore($(this));
            $(newInputfile).appendTo($(this).parents('.custom-table__cell').next('.custom-table__cell'));
        });
        //add  question
        $('.test-table-constructor').on('click', '.add-chapter-question', function () {
            var section = $(this).attr('data-section');
            var question = 1 + parseInt($(this).attr('data-question'));
            var newQuestion;
            if (section && question) {
                newQuestion =
                    '<div class="custom-table__row custom-table__edit-row flex j-s-b">' +
                    '    <div class="custom-table__cell">' +
                    '        <div class="input-group">' +
                    '            <div class="input-cont-constructor-test">' +
                    '            <span>' + section + '/' + question + '.</span>' +
                    '            <input type="text" placeholder="Введите ваш вопрос" class="input-shdw" id="question_' + section + '_' + question + '" name="question_' + section + '_' + question + '">' +
                    '            </div>' +
                    '            <p class="alert-error" style="display: none;">Заполните, пожалуйста, поле..</p>' +
                    '        </div>' +
                    '        <div class="input-group">' +
                    '            <input type="text" placeholder="Описание вопроса (необязательно)" class="input-shdw"  id="questionDesc_' + section + '_' + question + '" name="questionDesc_' + section + '_' + question + '">' +
                    '        </div>' +
                    '    </div>' +
                    '    <div class="custom-table__cell">' +
                    '        <select id="typeQuestion_' + section + '_' + question + '" name="typeQuestion_' + section + '_' + question + '">' +
                    '            <option value="0" selected>Один из списка</option>' +
                    '            <option value="1">Множественный выбор</option>' +
                    '            <option value="2">Шкала</option>' +
                    '            <option value="3">Закрытый "Да\Нет"</option>' +
                    '            <option value="4">Открытый вопрос</option>' +
                    '        </select>' +
                    '    </div>' +
                    '    <div class="custom-table__cell">' +
                    '        <label class="test-radio">' +
                    '            <input type="checkbox" name="questionRadio_' + section + '_' + question + '_1" id="questionRadio_' + section + '_' + question + '_1" checked>' +
                    '            <div class="test-radio__answer"></div>' +
                    '            <label>' +
                    '                <input type="text" class="answer input-shdw" value="" name="item_' + section + '_' + question + '_1" id="item_' + section + '_' + question + '_1" placeholder="Введите вариант ответа">' +
                    '            </label>' +
                    '            <input class="scores input-shdw" type="text" value="0" name="itemScore_' + section + '_' + question + '_1" id="itemScore_' + section + '_' + question + '_1">' +
                    '        </label>' +
                    '        <label class="test-radio">' +
                    '            <input type="checkbox" name="questionRadio_' + section + '_' + question + '_2" id="questionRadio_' + section + '_' + question + '_2" checked>' +
                    '            <div class="test-radio__answer"></div>' +
                    '            <label>' +
                    '                <input type="text" class="answer input-shdw" value=""  name="item_' + section + '_' + question + '_2" id="item_' + section + '_' + question + '_2" placeholder="Введите вариант ответа">' +
                    '            </label>' +
                    '            <input class="scores input-shdw" type="text" value="0" name="itemScore_' + section + '_' + question + '_2" id="itemScore_' + section + '_' + question + '_2">' +
                    '        </label>' +
                    '        <a href="#" class="circle-add add-answer add-questions-item" data-section="' + section + '" data-question="' + question + '" data-item="2">+</a>' +
                    '    </div>' +
                    '     <div class="custom-table__cell">' +
                    '       <div class="upload-answer">'+
                    '           <div class="input-group">'+
                    '                <input id="file-name_' + section + '_' + question + '_1" type="file" name="file-name_' + section + '_' + question + '_1" class="test-answer">'+
                    '               <label for="file-name_' + section + '_' + question + '_1"><img class="upload-image" src="../../../img/corp/icons/download-arrow_blue.png"></label>'+
                    '           </div>'+
                    '       </div>' +
                    '       <div class="upload-answer">'+
                    '            <div class="input-group">'+
                    '                <input id="file-name_' + section + '_' + question + '_2" type="file" name="file-name_' + section + '_' + question + '_2" class="test-answer">'+
                    '               <label for="file-name_' + section + '_' + question + '_2"><img class="upload-image" src="../../../img/corp/icons/download-arrow_blue.png"></label>'+
                    '           </div>'+
                    '       </div>' +
                    '    </div>' +
                    '    <div class="custom-table__cell">' +
                    '        <a href="#" title="Edit">' +
                    '            <img src="../../../img/corp/icons/pencil.png" alt="Edit">' +
                    '        </a>' +
                    '        <a href="#" title="Delete" class="delete-sections-question">' +
                    '            <img src="../../../img/corp/icons/trash.png" alt="Delete">' +
                    '        </a>' +
                    '        <a href="#" title="Move"  class="move-question">' +
                    '            <img src="../../../img/corp/icons/movetest.png" alt="Move">' +
                    '        </a>' +
                    '    </div>' +
                    '</div>';
                $(newQuestion).insertBefore($(this));
                $(this).attr('data-question', question);
                $('.test-table-constructor select').customSelect();
            }
        });
        //delete-chapter
        $('.test-table-constructor').on('click', '.delete-chapter', function () {
            if($(this).data('id')){
                $('#remove-course-popup .submit-remove-course').data('id', $(this).data('id'));
            }
            $('#remove-course-popup .submit-remove-course').data('type','chapter');
            $('#remove-course-popup .submit-remove-course').data('childId',$(this).parents('.custom-table__row').find('.title .input-shdw').attr('id'));
            if($(this).parents('.custom-table__row').find('.title input').val().length > 0) {
                $('#remove-course-popup h3').html('Вы действительно хотите удалить раздел «' + $(this).parents('.custom-table__row').find('.title input').val() + '» ?');
            }
            else {
                $('#remove-course-popup h3').html('Вы действительно хотите удалить раздел?');
            }
            $.magnificPopup.open({
                items: [
                    {
                        src: '#remove-course-popup'
                    }
                ]
            });
        });
        //delete-question
        $('.test-table-constructor').on('click', '.delete-sections-question', function () {
            if($(this).data('id')){
                $('#remove-course-popup .submit-remove-course').data('id', $(this).data('id'));
            }
            $('#remove-course-popup .submit-remove-course').data('type','sectionsQuestion');
            $('#remove-course-popup .submit-remove-course').data('childId',$(this).parents('.custom-table__row').find('.input-cont-constructor-test .input-shdw').attr('id'));
            if($(this).parents('.custom-table__row').find('.input-cont-constructor-test input').val().length > 0) {
                $('#remove-course-popup h3').html('Вы действительно хотите удалить вопрос «' + $(this).parents('.custom-table__row').find('.input-cont-constructor-test input').val() + '» ?');
            }
            else {
                $('#remove-course-popup h3').html('Вы действительно хотите удалить вопрос?');
            }
            $.magnificPopup.open({
                items: [
                    {
                        src: '#remove-course-popup'
                    }
                ]
            });
            // if (confirm("Вы уверены что хотите удалить вопрос?")) {
            //     var nextElements = $(this).parents('.custom-table__row').nextAll();
            //     nextElements.each(function (index, element) {
            //         var inputs = $(element).find('input');
            //         inputs.each(function (index, input) {
            //             prevName = $(input).attr('name').split("_");
            //             prevName[2] = prevName[2] - 1;
            //             newName = prevName.join('_');
            //             $(input).attr('name', newName);
            //             $(input).attr('id', newName);
            //         });
            //         var labels = $(element).find('label');
            //         labels.each(function (index, label) {
            //             if($(label).attr('for')){
            //                 prevName = $(label).attr('for').split("_");
            //                 prevName[2] = prevName[2] - 1;
            //                 newName = prevName.join('_');
            //                 $(label).attr('for', newName);
            //             }
            //         });
            //         var Selects = $(element).find('select');
            //         Selects.each(function (index, select) {
            //             prevName = $(select).attr('name').split("_");
            //             prevName[2] = prevName[2] - 1;
            //             newName = prevName.join('_');
            //             $(select).attr('name', newName);
            //             $(select).attr('id', newName);
            //         });
            //         var Links = $(element).find('.add-questions-item');
            //         Links.each(function (index, link) {
            //             question = -1 + parseInt($(link).attr('data-question'));
            //             $(link).attr('data-question', question);
            //         });
            //         var numbers = $(element).find('.input-cont-constructor-test span').text().split('/');
            //         if (numbers[1]) {
            //             numbers[1] = parseInt(numbers[1]) - 1;
            //             $(element).find('.input-cont-constructor-test span').html(numbers.join('/'));
            //         }
            //         if (element.hasAttribute('data-question')) {
            //             question = -1 + parseInt($(element).attr('data-question'));
            //             $(element).attr('data-question', question);
            //             return false;
            //         }
            //     });
            //     $(this).parents('.custom-table__row').remove();
            // }
        });
        $('.test-table-constructor').on('click', '.add-chapter-constructor', function () {
            var section = $(this).parents('.custom-table__body').find('.add-chapter-question').attr('data-question');
            var newChapter =
                '<div class="custom-table__body">' +
                '    <div class="custom-table__row flex j-s-b">' +
                '        <h4 class="title add-chapter">' +
                '            <a href="#" class="circle-add add-chapter-constructor">' +
                '                <img src="../../../img/corp/icons/chapter-img02.png" alt="">' +
                '             </a>' +
                '             <div class="title">' +
                '               <span>Раздел ' + section + ': </span>' +
                '               <input type="text" class="answer input-shdw" value="" placeholder="«Название»" id="section_' + section + '" name="section_' + section + '">' +
                '              </div>' +
                '              <div class="delete-chapter-cont">' +
                '               <a href="#" title="Delete" class="delete-chapter">' +
                '                   <img src="../../../img/corp/icons/trash.png" alt="Delete">' +
                '               </a>' +
                '              </div>' +
                '         </h4>' +
                '    </div>' +
                '    <a href="#" class="circle-add add-chapter-question" data-section="' + section + '" data-question="0">+</a>' +
                '</div>';
            $(newChapter).insertBefore($(this).parents('.custom-table__body'));
            section = 1;
            var Sections = $('.test-table-constructor').find('.custom-table__body');
            Sections.each(function (index, section_el) {
                var Questions = $(section_el).children();
                Questions.each(function (index, element) {
                    if ($(element).find('.title span')) {
                        $(element).find('.title span').text('Раздел ' + section + ': ');
                    }
                    var numbers = $(element).find('.input-cont-constructor-test span').text().split('/');
                    if (numbers[0]) {
                        numbers[0] = section;
                        $(element).find('.input-cont-constructor-test span').html(numbers.join('/'));
                    }
                    var inputs = $(element).find('input');
                    inputs.each(function (index, input) {
                        prevName = $(input).attr('name').split("_");
                        prevName[1] = section;
                        newName = prevName.join('_');
                        $(input).attr('name', newName);
                        $(input).attr('id', newName);
                    });
                    var labels = $(element).find('label');
                    labels.each(function (index, label) {
                        if($(label).attr('for')){
                            prevName = $(label).attr('for').split("_");
                            prevName[1] = section;
                            newName = prevName.join('_');
                            $(label).attr('for', newName);
                        }
                    });
                    var Selects = $(element).find('select');
                    Selects.each(function (index, select) {
                        prevName = $(select).attr('name').split("_");
                        prevName[1] = section;
                        newName = prevName.join('_');
                        $(select).attr('name', newName);
                        $(select).attr('id', newName);
                    });
                    var Links = $(element).find('.add-questions-item');
                    Links.each(function (index, link) {
                        $(link).attr('data-section', section);
                    });
                });
                var Links = $(section_el).find('.add-chapter-question');
                Links.each(function (index, link) {
                    $(link).attr('data-section', section);
                });
                section++;
            });
            $('.test-table-constructor .custom-table__body').sortable(
                {
                    appendTo: ".custom-table__body",
                    cancel: ".title, .circle-add, input, select, label",
                    axis: "y",
                    items: "> .custom-table__edit-row",
                    deactivate: function (event, ui) {
                        RefreshQuestionCostructor();
                    }
                }
            );
        });
        // select type of questions
        $('.test-table-constructor').on('change', 'select', function () {
            var nameQuestion = $(this).attr('name').split('_');
            var newInputfile;
            var section;
            var question;
            if (nameQuestion[1] && nameQuestion[2]) {
                section = nameQuestion[1];
                question = nameQuestion[2];
                var newQuestion;
                if ($(this).val() == 0)
                {
                    newQuestion =
                        '<label class="test-radio">' +
                        '    <input type="checkbox" name="questionRadio_' + section + '_' + question + '_1" id="questionRadio_' + section + '_' + question + '_1" checked>' +
                        '    <div class="test-radio__answer"></div>' +
                        '    <label>' +
                        '        <input type="text" class="answer input-shdw" value="" placeholder="Введите вариант ответа" name="item_' + section + '_' + question + '_1" id="item_' + section + '_' + question + '_1">' +
                        '    </label>' +
                        '    <input class="scores input-shdw" type="text" value="0" name="itemScore_' + section + '_' + question + '_1" id="itemScore_' + section + '_' + question + '_1">' +
                        '</label>' +
                        '<label class="test-radio">' +
                        '    <input type="checkbox" name="questionRadio_' + section + '_' + question + '_2" id="questionRadio_' + section + '_' + question + '_2" checked>' +
                        '    <div class="test-radio__answer"></div>' +
                        '    <label>' +
                        '        <input type="text" class="answer input-shdw" value="" placeholder="Введите вариант ответа" name="item_' + section + '_' + question + '_2" id="item_' + section + '_' + question + '_2">' +
                        '    </label>' +
                        '    <input class="scores input-shdw" type="text" value="0" name="itemScore_' + section + '_' + question + '_2" id="itemScore_' + section + '_' + question + '_2">' +
                        '</label>' +
                        '<a href="#" class="circle-add add-answer add-questions-item" data-section="' + section + '" data-question="' + question + '" data-item="2">+</a>';
                        newInputfile = 
                        '       <div class="upload-answer">'+
                        '           <div class="input-group">'+
                        '                <input id="file-name_' + section + '_' + question + '_1" type="file" name="file-name_' + section + '_' + question + '_1" class="test-answer">'+
                        '               <label for="file-name_' + section + '_' + question + '_1"><img class="upload-image" src="../../../img/corp/icons/download-arrow_blue.png"></label>'+
                        '           </div>'+
                        '       </div>' +
                        '       <div class="upload-answer">'+
                        '            <div class="input-group">'+
                        '                <input id="file-name_' + section + '_' + question + '_2" type="file" name="file-name_' + section + '_' + question + '_2" class="test-answer">'+
                        '               <label for="file-name_' + section + '_' + question + '_2"><img class="upload-image" src="../../../img/corp/icons/download-arrow_blue.png"></label>'+
                        '           </div>'+
                        '       </div>';
                }
                if ($(this).val() == 1) {
                    newQuestion =
                        '<label class="test-radio">' +
                        '    <input type="checkbox" name="questionRadio_' + section + '_' + question + '_1" id="questionRadio_' + section + '_' + question + '_1" checked>' +
                        '    <div class="test-radio__answer"></div>' +
                        '    <label>' +
                        '        <input type="text" class="answer input-shdw" placeholder="Введите вариант ответа" name="item_' + section + '_' + question + '_1" id="item_' + section + '_' + question + '_1">' +
                        '    </label>' +
                        '    <input class="scores input-shdw" type="text" value="0" name="itemScore_' + section + '_' + question + '_1" id="itemScore_' + section + '_' + question + '_1">' +
                        '</label>' +
                        '<label class="test-radio">' +
                        '    <input type="checkbox" name="questionRadio_' + section + '_' + question + '_2" id="questionRadio_' + section + '_' + question + '_2" checked>' +
                        '    <div class="test-radio__answer"></div>' +
                        '    <label>' +
                        '        <input type="text" class="answer input-shdw" placeholder="Введите вариант ответа" name="item_' + section + '_' + question + '_2" id="item_' + section + '_' + question + '_2">' +
                        '    </label>' +
                        '    <input class="scores input-shdw" type="text" value="0" name="itemScore_' + section + '_' + question + '_2" id="itemScore_' + section + '_' + question + '_2">' +
                        '</label>' +
                        '<a href="#" class="circle-add add-answer add-questions-item-check" data-section="' + section + '" data-question="' + question + '" data-item="2">+</a>';
                        newInputfile = 
                        '       <div class="upload-answer">'+
                        '           <div class="input-group">'+
                        '                <input id="file-name_' + section + '_' + question + '_1" type="file" name="file-name_' + section + '_' + question + '_1" class="test-answer">'+
                        '               <label for="file-name_' + section + '_' + question + '_1"><img class="upload-image" src="../../../img/corp/icons/download-arrow_blue.png"></label>'+
                        '           </div>'+
                        '       </div>' +
                        '       <div class="upload-answer">'+
                        '            <div class="input-group">'+
                        '                <input id="file-name_' + section + '_' + question + '_2" type="file" name="file-name_' + section + '_' + question + '_2" class="test-answer">'+
                        '               <label for="file-name_' + section + '_' + question + '_2"><img class="upload-image" src="../../../img/corp/icons/download-arrow_blue.png"></label>'+
                        '           </div>'+
                        '       </div>';
                }
                if ($(this).val() == 2) {
                    newQuestion =
                        '<div class="input-group test-scale">' +
                        '    <span>От <input type="number" min="1" max="10" value="1" name="questionScale_' + section + '_' + question + '_1" id="questionScale_' + section + '_' + question + '_1"></span>' +
                        '    <input type="text" class="input-shdw answer" placeholder="Подпись (необязательно)" name="questionScaleDesc_' + section + '_' + question + '_1" id="questionScaleDesc_' + section + '_' + question + '_1">' +
                        '    <input class="scores input-shdw" type="text" value="0" name="itemScore_' + section + '_' + question + '_1" id="itemScore_' + section + '_' + question + '_1">' +
                        '</div>' +
                        '<div class="input-group test-scale">' +
                        '    <span>До <input type="number" min="1" max="10" value="10" name="questionScale_' + section + '_' + question + '_2" id="questionScale_' + section + '_' + question + '_2"></span>' +
                        '    <input type="text" class="input-shdw answer" placeholder="Подпись (необязательно)" name="questionScaleDesc_' + section + '_' + question + '_2" id="questionScaleDesc_' + section + '_' + question + '_2">' +
                        '    <input class="scores input-shdw" type="text" value="10" name="itemScore_' + section + '_' + question + '_2" id="itemScore_' + section + '_' + question + '_2">' +
                        '</div>';
                        newInputfile = ' ';
                }
                if ($(this).val() == 3) {
                    newQuestion =
                        '<label class="test-radio">' +
                        '    <input type="checkbox" name="questionYesNot_' + section + '_' + question + '" id="questionYesNot_' + section + '_' + question + '_1" checked>' +
                        '    <div class="test-radio__answer answer-yes_not"><span class="answer">Да</span><input class="scores input-shdw" type="text" value="0" name="itemScore_' + section + '_' + question + '_1" id="itemScore_' + section + '_' + question + '_1"></div>' +
                        '</label>' +
                        '<label class="test-radio">' +
                        '    <input type="checkbox" name="questionYesNot_' + section + '_' + question + '" id="questionYesNot_' + section + '_' + question + '_2" checked>' +
                        '    <div class="test-radio__answer answer-yes_not"><span class="answer">Нет</span><input class="scores input-shdw" type="text" value="100" name="itemScore_' + section + '_' + question + '_2" id="itemScore_' + section + '_' + question + '_2"></div>' +
                        '</label>';
                        newInputfile = ' ';
                }
                if ($(this).val() == 4) {
                    newQuestion =
                        '<div class="input-group">' +
                        '    <label>' +
                        '        <input type="text" class="input-shdw answer open-question" value="15+ символов" name="item_' + section + '_' + question + '" id="item_' + section + '_' + question + '">' +
                        '    </label>' +
                        '</div>';
                        newInputfile = '';
                }
                if (newQuestion) {
                    $(this).parents('.custom-table__cell').next('.custom-table__cell').html(newQuestion);
                    if(newInputfile) {
                        $(this).parents('.custom-table__cell').next('.custom-table__cell').next('.custom-table__cell').html(newInputfile);
                    }
                    else {
                        $(this).parents('.custom-table__cell').next('.custom-table__cell').next('.custom-table__cell').html(' ');
                    }
                }
            }
        });
        // delete question
        $('.submit-remove-course').click(function(){
            var childId = $(this).data('childId');
            if($(this).data('type') == 'sectionsQuestion'){
                var nextElements = $('#' + childId).parents('.custom-table__row').nextAll();
                nextElements.each(function (index, element) {
                    var inputs = $(element).find('input');
                    inputs.each(function (index, input) {
                        if($(input).attr('name')){
                            prevName = $(input).attr('name').split("_");
                            prevName[2] = prevName[2] - 1;
                            newName = prevName.join('_');
                            $(input).attr('name', newName);
                            $(input).attr('id', newName);
                        }
                    });
                    var labels = $(element).find('label');
                    labels.each(function (index, label) {
                        if($(label).attr('for')){
                            prevName = $(label).attr('for').split("_");
                            prevName[2] = prevName[2] - 1;
                            newName = prevName.join('_');
                            $(label).attr('for', newName);
                        }
                    });
                    var Selects = $(element).find('select');
                    Selects.each(function (index, select) {
                        if($(select).attr('name')){
                            prevName = $(select).attr('name').split("_");
                            prevName[2] = prevName[2] - 1;
                            newName = prevName.join('_');
                            $(select).attr('name', newName);
                            $(select).attr('id', newName);
                        }
                    });
                    var Links = $(element).find('.add-questions-item');
                    Links.each(function (index, link) {
                        question = -1 + parseInt($(link).attr('data-question'));
                        $(link).attr('data-question', question);
                    });
                    var numbers = $(element).find('.input-cont-constructor-test span').text().split('/');
                    if (numbers[1]) {
                        numbers[1] = parseInt(numbers[1]) - 1;
                        $(element).find('.input-cont-constructor-test span').html(numbers.join('/'));
                    }
                    if (element.hasAttribute('data-question')) {
                        question = -1 + parseInt($(element).attr('data-question'));
                        $(element).attr('data-question', question);
                        return false;
                    }
                });
                $('#' + childId).parents('.custom-table__row').remove();
            }
            if($(this).data('type') == 'chapter') {
                $('#' + childId).parents('.custom-table__body').remove();
                section = 1;
                var Sections = $('.test-table-constructor').find('.custom-table__body');
                Sections.each(function (index, section_el) {
                    var Questions = $(section_el).children();
                    Questions.each(function (index, element) {
                        if ($(element).find('.title span')) {
                            $(element).find('.title span').text('Раздел ' + section + ': ');
                        }
                        var numbers = $(element).find('.input-cont-constructor-test span').text().split('/');
                        if (numbers[0]) {
                            numbers[0] = section;
                            $(element).find('.input-cont-constructor-test span').html(numbers.join('/'));
                        }
                        var inputs = $(element).find('input');
                        inputs.each(function (index, input) {
                            prevName = $(input).attr('name').split("_");
                            prevName[1] = section;
                            newName = prevName.join('_');
                            $(input).attr('name', newName);
                            $(input).attr('id', newName);
                        });
                        var labels = $(element).find('label');
                        labels.each(function (index, label) {
                            if($(label).attr('for')){
                                prevName = $(label).attr('for').split("_");
                                prevName[1] = section;
                                newName = prevName.join('_');
                                $(label).attr('for', newName);
                            }
                        });
                        var Selects = $(element).find('select');
                        Selects.each(function (index, select) {
                            prevName = $(select).attr('name').split("_");
                            prevName[1] = section;
                            newName = prevName.join('_');
                            $(select).attr('name', newName);
                            $(select).attr('id', newName);
                        });
                        var Links = $(element).find('.add-questions-item');
                        Links.each(function (index, link) {
                            $(link).attr('data-section', section);
                        });
                    });
                    var Links = $(section_el).find('.add-chapter-question');
                    Links.each(function (index, link) {
                        $(link).attr('data-section', section);
                    });
                    section++;
                });
            }
        });
    });
});